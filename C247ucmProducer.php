<?php

namespace consulting247;

/**
 * Description of c247ucmProducer
 *
 * @author ken
 */
class C247ucmProducer {
     private $mode;
     private $results;

     function __construct($data, $mode = "live") {
          $this->mode = $mode;
          //should data be sanitized?
          //get the host name that this is being sent from
          $data['c247HostName']=  filter_input(INPUT_SERVER, 'HTTP_HOST',FILTER_SANITIZE_STRING);
          $str_data = json_encode($data);
          switch (strtolower($mode)) {
               case("live"):
               case("test"):
                    $url_send = "https://development.consulting247.info/Contact/index/xxxxx";
                    break;
               case("localhost"):
                    $url_send = "https://localhost/c247ucm/public/Contact/index/xxxxx";
                    break;
               default:
                    throw new \Exception("Mode value must be localhost, test, or live ");
          }
          $this->results = $this->sendPostData($url_send, $str_data);
     }

     function sendPostData($url, $post) {

          try {
               $ch = curl_init($url);
               if (FALSE === $ch)
                    throw new Exception('failed to initialize');

               curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
               //turn of verication on my local host machine, testing for now remove later
               if ($this->mode === 'localhost') {
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    // end if localhost*/
               }
               curl_setopt($ch, CURLOPT_URL, $url);


               $result = curl_exec($ch);
               if (FALSE === $result) {
                    throw new Exception(curl_error($ch), curl_errno($ch));
               }

               // ...process $content now
          } catch (Exception $e) {

               trigger_error(sprintf(
                         'Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
          }
          curl_close($ch);  // Seems like good practice
          return $result;
     }

     function getResults() {
          return $this->results;
     }

     function getStatus() {
          $jsonObject = json_decode($this->results);
          if (json_last_error() !== JSON_ERROR_NONE) {
               return false;
          }

          return $jsonObject->status;
     }

     function getJsonObject() {
          $jsonObject = json_decode($this->results);
          if (json_last_error() !== JSON_ERROR_NONE) {
               return false;
          }

          return $jsonObject;
     }

}
